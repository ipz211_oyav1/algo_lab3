﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Task2
{
    internal class Program
    {
       
        static void Main(string[] args)
        {
            Random rnd = new Random();
            
            int[] arr = new int[5];
            Console.Write("Набiр бiтiв ->");
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(2);
                Console.Write(arr[i]);
            }
            Console.WriteLine();
            Stopwatch timer = new Stopwatch();
            timer.Start();

            int ii = arr.Length-1;
            double number = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 1)
                {
                    number += Math.Pow(2, ii);
                    ii -= 1;
                }
            }

            timer.Stop();

            TimeSpan timeTaken = timer.Elapsed;

            string foo = timeTaken.Ticks.ToString();
            //Console.WriteLine(foo);
            Console.WriteLine("Максимально можливе число ->" + number);
            
            Console.ReadLine();
        }
    }
}
