﻿#include <stdio.h>
#include <math.h>
double factorial(int x)
{
    return(x < 2) ? 1 : x * factorial(x - 1);
}
int main()
{
    double   x, y, b = 50;
    for (x = 1; x <= b; x += 1)
    {
        y = x;
        printf("f(%.0lf) = %.0lf\t", x, y);
        y = log(x);
        printf("f(log(%.0lf)) = %-7.2lf\t", x, y);
        y = x * log(x);
        printf("f(%.0lf*log(%.0lf)) = %-7.2lf\t", x, x, y);
        y = pow(x, 2);
        printf("f(%.0lf^2) = %-14.0lf", x, y);
        y = pow(2, x);
        printf("f(2^%.0lf) = %-18.0lf\t", x, y);
        y = factorial(x);
        printf("f(%.0lf!) = %.0lf\n", x, y);
    }
    return 0;
}

